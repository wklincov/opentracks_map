#!/usr/bin/env python3

from .xml_parser import Track, parse_kmz
from .utils import logger

from pathlib import Path
from math import floor
from typing import Dict

from pytz import utc

import pygal

from fastapi import FastAPI
from fastapi.requests import Request
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles

from starlette.config import Config
from starlette.requests import Request
from starlette.middleware.sessions import SessionMiddleware
from starlette.responses import HTMLResponse, JSONResponse, RedirectResponse

from authlib.integrations.starlette_client import OAuth

from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore
from apscheduler.triggers.interval import IntervalTrigger
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from apscheduler.executors.asyncio import AsyncIOExecutor


app = FastAPI()
app.add_middleware(SessionMiddleware, secret_key='!secret')
app.mount("/static", StaticFiles(directory="opentracks_map/static"), name="static")

templates = Jinja2Templates(directory='opentracks_map/templates')



jobstores = {
    'default': SQLAlchemyJobStore(url='sqlite:///jobs.sqlite')
}
executors = {
    'default': AsyncIOExecutor(),
}
scheduler = AsyncIOScheduler(jobstores=jobstores, executors=executors, timezone=utc)


tracks: Dict[Path, Track] = {}


oauth = OAuth(config=Config('.env'))
oauth.register(
    name='gitlab',
    server_metadata_url='https://gitlab.com/.well-known/openid-configuration',
    client_kwargs={
        'scope': 'openid email'
    }
)


@app.on_event('startup')
async def startup():
    parse_kmz_files()

    interval_trigger = IntervalTrigger(minutes=20)
    _ = scheduler.add_job(parse_kmz_files, interval_trigger)
    scheduler.start()


@app.on_event('shutdown')
async def shutdown():
    scheduler.shutdown()


def parse_kmz_files():
    logger.info('Running schedule.')

    for f in Path.cwd().glob('data/*.kmz'):
        track_name = f.name

        if track_name in tracks:
            continue

        logger.info(f'Adding new track: {track_name}.')
        res = parse_kmz(f)
        tracks[track_name] = res


@app.get('/', response_class=HTMLResponse)
async def root(request: Request):
    user = request.session.get('user')
    if user is None:
        return RedirectResponse(url='/login')

    data = {'request': request}

    return templates.TemplateResponse('index.html', data)


@app.get('/tracks', response_class=JSONResponse)
async def get_tracks(request: Request):
    user = request.session.get('user')
    if user is None:
        return RedirectResponse(url='/login')

    data = {name: track.to_json() for name, track in tracks.items()}

    return JSONResponse(data)


@app.get('/login') 
async def login(request: Request):
    remote = oauth.create_client('gitlab')
    redirect_uri = request.url_for('auth')

    return await remote.authorize_redirect(request, redirect_uri)


@app.route('/auth')
async def auth(request: Request):
    remote = oauth.create_client('gitlab')

    token = await remote.authorize_access_token(request)
    user = await remote.parse_id_token(request, token)

    logger.info(f'User logged in {user}.')

    request.session['user'] = dict(user)

    return RedirectResponse(url='/')


@app.get('/meta/')
async def meta(request: Request, name: str, width: float, height: float):
    for _, track in tracks.items():
        if track.name == name:
            break
    else:
        logger.warn(f'No track found: `{name}`.')
        return {}

    time, speed, heart_rate = track.get_meta()

    args = dict(
        x_label_rotation=20, show_dots=True,
        show_minor_x_labels=False,
        width=width, height=height, explicit_size=True,
        style=pygal.style.BlueStyle
    )

    chart = pygal.Line(**args)
    chart.title = name
    chart.x_labels = list(map(lambda d: d.strftime('%H:%M:%S'), time))
    count_labels_major = floor(len(time)*1/3)
    chart.x_labels_major = list(map(lambda d: d.strftime('%H:%M:%S'), time[0::count_labels_major]))

    chart.add('velocity', speed, show_only_major_dots=True)
    chart.add('heart rate', heart_rate, show_only_major_dots=True, secondary=True)

    data = chart.render_data_uri()

    context = {'request': request, 'data': data}
    return templates.TemplateResponse('linechart.j2', context)
