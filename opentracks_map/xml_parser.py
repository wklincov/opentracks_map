from .utils import T, grouped, get_avg, generate_color, logger

import geopy.distance
import arrow
import datetime
import zipfile
from typing import Tuple, List, Union
from lxml import etree
from dataclasses import dataclass, field


@dataclass
class Schema:
    type_: Union[float]
    name: str = 'unnamed'
    enabled: bool = False

    whitelisted_schemas = {'speed': 'Speed (m/s)', 'heart_rate': 'Heart rate (bpm)'}


@dataclass
class Point:
    time: datetime.datetime
    lat: float
    lon: float
    alt: float
    heart_rate: float
    speed: float


@dataclass
class Track:
    name: str = field(default_factory=str)
    schemas: List[Schema] = field(default_factory=list)
    points: List[Point] = field(default_factory=list)
    distance: float = field(default_factory=float)

    def get_coordinates(self) -> List[Tuple[float, float]]:
        coords = []
        for point in self.points:
            coords.append([point.lon, point.lat])

        return coords

    def get_meta(self) -> Tuple[List[datetime.datetime], List[float], List[float]]:
        speed = [point.speed * 3.6 for point in self.points]
        heart_rate = [point.heart_rate for point in self.points]
        time = [point.time for point in self.points]

        return time, speed, heart_rate

    def get_duration(self) -> float:
        end_time = self.points[-1].time
        start_time = self.points[0].time

        delta = end_time - start_time

        return str(delta - datetime.timedelta(microseconds=delta.microseconds))

    def get_avg_speed(self) -> float:
        return get_avg([point.speed * 3.6 for point in self.points])

    def get_avg_heart_rate(self) -> float:
        return get_avg([point.heart_rate for point in self.points])

    def get_color(self) -> str:
        return generate_color(self.name)

    def to_json(self) -> dict:
        return {
            'name': self.name,
            'coordinates': self.get_coordinates(),
            'color': self.get_color(),
            'distance': self.distance,
            'duration': self.get_duration(),
            'avg_heart_rate': self.get_avg_heart_rate(),
            'avg_speed': self.get_avg_speed()
        }


def parse_kmz(file_name: str) -> Track:
    track = Track()

    print(f'Opening {file_name}')
    with zipfile.ZipFile(file_name) as zip_file:
        with zip_file.open("doc.kml") as kml_file:
            parser = etree.XMLParser(strip_cdata=False)
            root = etree.parse(kml_file, parser).getroot()

    expr = '//*[local-name() = $name]'
    schema = root.xpath(expr, name='Schema')
    if len(schema) != 1:
        logger.info('More or less than 1 schema found, quitting.')

    schema = schema[0]

    schemas = schema.xpath(expr, name='SimpleArrayField')
    for schema in schemas:
        name = schema.attrib['name']
        schema_type = eval(schema.attrib['type'])

        if name in Schema.whitelisted_schemas:
            display = schema.getchildren()
            if not display:
                logger.error('More or lass than 1 displayName for schema found, quitting.')
                exit(1)

            display = display[0].text
            if display != Schema.whitelisted_schemas[name]:
                logger.error(f'Wrong displayName `{display}` for schema {name}, quitting.')
                exit(1)

            track.schemas.append(Schema(type_=schema_type, name=schema, enabled=True))
        else:
            logger.warn(f'{name} is not a know schema.')

    placemark = root.xpath(expr, name='Placemark')
    if len(placemark) != 1:
        logger.error('More or less than 1 placemark found, quitting.')
        exit(1)
    placemark = placemark[0]

    name = placemark.xpath(expr, name='name')[0].text
    if not name:
        logger.error('More or less than 1 name found, quitting.')
        exit(1)

    track.name = name
    logger.info(f'{track.name=}')

    multitrack = placemark.xpath(expr, name='MultiTrack')
    if len(multitrack) != 1:
        logger.info('More or less than 1 multitrack found, quitting.')
        exit(1)

    multitrack = multitrack[0]

    multi_expr = '//*[local-name() = $name]/*'
    tracks = multitrack.xpath(expr, name='Track')
    track_coords = []
    for single_track in tracks:
        track_coords.extend([
            child for child in single_track.iterchildren()
            if any(val in child.tag for val in ['when', 'coord'])
        ])
    track_coords = grouped(track_coords)

    ext = multitrack.xpath(expr, name='ExtendedData')[0]
    schemadata = ext.xpath(expr, name='SchemaData')[0]
    class_expr = '//*[local-name() = $name and @name = $tag_name]/*'
    speeds = schemadata.xpath(class_expr, name='SimpleArrayData', tag_name='speed')

    def to_float(number):
        if number is None:
            return 0.0
        else:
            return float(number)

    if not speeds:
        speeds = [0] * len(track_coords)
    else:
        speeds = [to_float(speed.text) for speed in speeds]

    heart_rates = schemadata.xpath(class_expr, name='SimpleArrayData', tag_name='heart_rate')
    if not heart_rates:
        heart_rates = [0] * len(track_coords)
    else:
        heart_rates = [to_float(heart_rate.text) for heart_rate in heart_rates]

    prev_lon = prev_lat = avg_lat = avg_lon = set_size = 0.0
    for when, coord in track_coords:
        heart_rate = heart_rates.pop(0)
        if not heart_rate:
            heart_rate = 0
        
        if len(speeds) > 1:
            speed = speeds.pop(0)
            if not speed:
                speed = 0

        coord_text = coord.text
        if not coord_text:
            continue

        lon, lat, alt = map(lambda x: float(x), coord_text.split(' '))

        if lon and lat:
            avg_lon += lon
            avg_lat += lat
            set_size += 1

        if lon and lat and set_size == 5:
            avg_lon /= set_size
            avg_lat /= set_size

            if prev_lon and prev_lat:
                diff = geopy.distance.distance((prev_lon, prev_lat), (avg_lon, avg_lat)).km
                if diff > 0.1:
                    logger.debug(f'{diff=} {avg_lon=} {avg_lat=} {prev_lon=} {prev_lat=}')
                track.distance += diff

            prev_lon, prev_lat = avg_lon, avg_lat
            avg_lon, avg_lat = 0.0, 0.0
            set_size = 0

        time = arrow.get(when.text)

        track.points.append(
            Point(time=time, lat=lat, lon=lon, alt=alt, heart_rate=heart_rate, speed=speed)
        )


    return track
