import logging
from typing import List, Tuple, Iterable, TypeVar


logging.config.fileConfig('logging.conf', disable_existing_loggers=False)
logger = logging.getLogger(__name__)


def generate_color(name: str) -> str:
    r, g, b = 0, 0, 0
    for chr in name:
        r = ord(chr) + ((r << 2) - r)
        g = ord(chr) + ((g << 3) - g)
        b = ord(chr) + ((b << 1) - b)

    r %= 256
    g %= 256
    b %= 256

    return f'#{r:0>2X}{g:0>2X}{b:0>2X}'


T = TypeVar("T")


def grouped(iterable: Iterable[T], n=2) -> Iterable[Tuple[T, ...]]:
    """s -> (s0,s1,s2,...sn-1), (sn,sn+1,sn+2,...s2n-1), ..."""
    return list(zip(*[iter(iterable)] * n))


def get_avg(lst: List[float]) -> float:
    return round(
        sum(lst)/len(lst), 2
    )
