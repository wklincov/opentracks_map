FROM docker.io/python:3.10-slim

RUN useradd -ms /bin/bash mapper && \
    mkdir /app && \
    chown -R mapper:mapper /app && \
    pip install poetry -U

USER mapper

EXPOSE 8000

COPY --chown=mapper:mapper pyproject.toml poetry.lock /app/

WORKDIR /app

RUN poetry install

COPY --chown=mapper:mapper . /app/

# To make sure uvicorn can access static files with https
ENV FORWARDED_ALLOW_IPS="*"

CMD ["poetry", "run", "uvicorn", "opentracks_map.main:app", "--host", "0.0.0.0", "--port", "8000"]
