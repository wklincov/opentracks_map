# opentracks-map

Run a webserver visualizing [opentracks](https://github.com/OpenTracksApp/OpenTracks) data.

## Run webserver

Build:

```bash
podman-compose build
```

Run:

```bash
# changes paths to opentracks data
cp docker-compose.yml{.sample,}
vim docker-compose.yml

# add `GITLAB_*` OAuth variables
copy .env{.sample,}
vim .env

podman-compose up
```

Open:

```bash
firefox http://localhost:8000
```

## TODO

- [ ] Read from config
- [ ] Allow upload of tracks
- [ ] Save tracks in a database
- [ ] Show speed information when highlighting tracks